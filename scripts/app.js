var app = angular.module("myApp", ['ngRoute']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/AddProducts', {
            templateUrl: "add_products.html",
            controller: 'productController'
        }).
        when('/ViewSalesRecords', {
            templateUrl: 'view-sales-records.html',
            controller: 'salesRecordController'
        }).
        when('/ViewSalesReport', {
            templateUrl: 'view-sales-report.html',
            controller: 'reportController'
        }).
        when('/ViewSingleSale', {
            templateUrl: 'view-single-sale.html',
            controller: 'salesRecordController'

        });

});

app.controller('salesRecordController', ['$scope', '$http', function ($scope, $http) {
 
    //removes both the product info and the quantity arrays from the insert object
    $scope.Delete = function (sale_id) {
        if (confirm("Are You sure you want to delete this record?"))
		{
			
			$http.post('./delete_sale.php',{'sale_id' : sale_id
			})
			.then(function(response)
			{
                $scope.deleteResponse = response.data;
                location.reload();
			})
		}
    }
	
    $scope.hideDelete = true;
    
    $http.get("read_sale.php")
        .then(
            function (response) {
                $scope.salesRecords = response.data;
            }
        ) 	
}]);


app.controller('reportController', ['$scope', '$http', function ($scope, $http) {
    //logic for reporting goes here
}]);



app.controller("productController", ['$scope', '$http', function ($scope, $http) {
    $scope.salesTable = false;
    $scope.isDisabled = true;
    $scope.isConfirmed = false;
    $scope.insert = {};
    $scope.insert.sales = [];
    $scope.insert.quantity = [];
    $scope.canEdit = [];

    //getting producs from database
    $http.get("display-products.php")
        .then(
            function (response) {
                $scope.products = response.data;
            }
        )

    //Adds two arrays to the Object "insert", first array is the products (id, name, price), second array is the quantity    
    $scope.onAdd = function (product) {
        var index = $scope.insert.sales.indexOf(product);
        if (index == -1) {
            $scope.insert.sales.push(product);
            if (angular.element(document.getElementById(product.product_id + product.product_name)).val() == 0) {
                $scope.insert.quantity.push(1);
                $scope.canEdit.push(false);
            } else {
                var num = (angular.element(document.getElementById(product.product_id + product.product_name)).val());
                $scope.insert.quantity.push(num);
                $scope.canEdit.push(false);
            }
            $scope.salesTable = true;
        }
    }

    //sets onEdit array at the specified index (from html) to true, so that products quantity can be changed before submission
    $scope.onEdit = function (index) {
        $scope.canEdit[index] = true;
    }

    //disbabled the number input and hides the confirm button
    $scope.onConfirm = function (product, index) {
        $scope.canEdit[index] = false;
        $scope.insert.quantity[index] = Number(angular.element(document.getElementById(product.product_id + product.product_name + product.product_id)).val());
        $scope.getTotal();
    }

    //removes both the product info and the quantity arrays from the insert object
    $scope.onDelete = function (product) {
        var index = $scope.insert.sales.indexOf(product);
        $scope.insert.sales.splice(index, 1);
        $scope.insert.quantity.splice(index, 1);
        $scope.getTotal();
    }
    //Get the total
    $scope.getTotal = function () {
        var total = 0;
        for (var i = 0; i < $scope.insert.sales.length; i++) {
            total += (Number($scope.insert.quantity[i]) * Number($scope.insert.sales[i].product_price));
        }
        return total;
    }

    //should add the final sale to the database, still figuring this out. 
    $scope.submitForm = function (Data, Quantity) {
        Number(Quantity);
        var data = [];
        for (var i = 0; i < Data.length; i++) {
            data[i] = {
                productId: Data[i].product_id,
                //productName: Data[i].productName,
                //price: Data[i].price,
                quantity: Quantity[i],
            }
        }


        //get method. Still working on this
        $http({
            method: "POST",
            url: "./create_sale.php",
            headers: {
                'Content-Type': 'application/x-form-urlencoded; charset=UTF-8'
            },
            data: data
            // data: {productId: 1, quantity: 2}
        }).then(function (response) {
            // get POST response
            $scope.postResponse = response.data;
            // console.log(data);
            // for each item in sale, decrease stock by quantity
            for (i = 0; i < data.length; i++) {
                // get index of product from item in sale - 1
                $scope.products[data[i].productId - 1].product_stock -= data[i].quantity;
            }

        });
        $scope.insert.sales = [];
        $scope.insert.quantity = [];
    }




}]);

