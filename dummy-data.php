<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<strong>WARNING: Repopulating the database will clear all records first. All current data will be lost.</strong>

	<form method="POST">
		<p><button name="action" type="submit" value="repopulate">Repopulate Sales</button>
		The repopulate sales button will delete all sales from the database and generate new random sales records.</p>
		
		<p><button name="action" type="submit" value="restock">Restock Products</button>
		The restock products will increase each product's stock by 10.</p>
	</form>
</body>
</html>


<?php

	// db config and objects
	include_once 'config/database.php';
	include_once 'objects/sale.php';

	// db conection
	$database = new Database();
	$conn = $database->getConnection();


	$number_of_sales = 1000;
	$restock_amount = 10;
	$max_product_quantity = 10;


	if (isset($_POST["action"]) && $_POST["action"] == "restock") {
		// echo "restocking";

		// update stock value of every product to stock + 10
		$query = "UPDATE products SET product_stock=product_stock + $restock_amount";
		$result = $conn->query($query);

		if ($result) {
			echo "<p>Restocking successful: increased all product stock by $restock_amount";
		}
		else {
			echo "<p>Restocking failed: $conn->error";
		}
	}
	elseif (isset($_POST["action"]) && $_POST["action"] == "repopulate") {
		// echo "repopulating";

		
		$sales_cleared = FALSE;

		// very ugly and convoluted nested query to clear sales and sales_details tables
		$query = "SET FOREIGN_KEY_CHECKS = 0";

		if ($conn->query($query)) {
			$query = "TRUNCATE TABLE sales_details";

			if ($conn->query($query)) {
				// echo 'success<br>';
				$query = "TRUNCATE TABLE sales;";

				if ($conn->query($query)) {
					// echo 'truncated products<br>';

					$query = "SET FOREIGN_KEY_CHECKS = 1";

					if ($conn->query($query)) {
						$sales_cleared = TRUE;
						// echo "cleared sales<br>";
					} 
					else echo "Error clearing database: $conn->error";
				}
				else echo "Error clearing database: $conn->error";
			} 
			else echo "Error clearing database: $conn->error";
		}
		else echo "Error clearing database: $conn->error";


		// if sales data has been cleared
		if ($sales_cleared) {

			// arrays to hold product data from db
			$products = array();
			$product_stock = array();

			// initialize queries to add sales and details
			$salesQuery 	= "INSERT INTO sales (sale_id, sale_date) VALUES";
			$detailsQuery 	= "INSERT INTO sales_details (sale_id, product_id, quantity) VALUES";

			// query to get product data
			$query = "SELECT product_id, product_stock FROM products";
			$result = $conn->query($query);

			// if successful query, push results to product arrays
			if ($result) {
				while ($r = $result->fetch_assoc()) {
					$products[] = $r["product_id"];
					$product_stock[] = $r["product_stock"];
				}
			}
			else echo "Error retrieving product list: $conn->error";

			// loop once for each sale
			for ($i = 1; $i <= $number_of_sales; $i++) {

				// initialise query to insert into sales table
				$salesQuery .= "($i, CURRENT_TIMESTAMP - INTERVAL FLOOR(RAND() * 180) DAY), ";

				// do while - 75% chance, get random product and qty
				do {
					$id = mt_rand(1, count($products));
					$qty = mt_rand(1, $max_product_quantity);
					
					// if the sale_id + product_id are not found in query, continue
					// this ensures that the same product is not added more than once for each
					if (!strpos($detailsQuery, "($i, $id") !== false) {
						// add sales detail to query string
						$detailsQuery .= "($i, $id, $qty), ";
					}
				} while (mt_rand(0, 100) > 25);
			}

			// remove trailing ", " on each query
			$salesQuery = substr($salesQuery, 0, strlen($salesQuery) - 2);
			$detailsQuery = substr($detailsQuery, 0, strlen($detailsQuery) - 2);


			// echo "<br><br>$salesQuery";
			// echo "<br>$detailsQuery";
			// echo "<br><br>";


			// if sales query is successful, execute sales details query
			if ($conn->query($salesQuery)) {
				if ($conn->query($detailsQuery)) {
					echo "$number_of_sales sales successfully generated.";
				}
				else {
					echo "error generating sales details: $conn->error";
				}
			}
			else {
				echo "error generating sales: $conn->error";
			}
		}

	}






?>
