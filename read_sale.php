<?php

	// db config and objects
	include_once './config/database.php';
	include_once './objects/sale.php';

	// db conection
	$database = new Database();
	$conn = $database->getConnection();

	// create sale object to interact with Sales table
	$sale = new Sale($conn);

	// if sale id is provided, retrieve that record
	// else retrieve all records
	if (isset($_POST['sale_id'])) {
		$sale->id = $_POST['sale_id'];
		$sale->read();
	}
	else {		
		$sale->displaySalesRecords();
	}



?>