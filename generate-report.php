
<!DOCTYPE html>
<html>
<head>
	<title>PHP Generate Sales Reports</title>
</head>
<body>

	<h1>Sales Report Generator</h1>

	<p>Select a month from the list and click the 'Generate' button to generate a sales report.</p>
	
	<form>
		<select name="month">
			<option value="01">January</option>
			<option value="02">February</option>
			<option value="03">March</option>
			<option value="04">April</option>
			<option value="05">May</option>
			<option value="06">June</option>
			<option value="07">July</option>
			<option value="08">August</option>
			<option value="09">September</option>
			<option value="10">October</option>
			<option value="11">November</option>
			<option value="12">December</option>
		</select>

		<button>Generate</button>
	</form>
	<form method="post" action="export.php">
		<input type="hidden" name="month" value="<?php if (isset($_GET['month'])) echo $_GET['month']; ?>" />
		<input type="submit" name="export" value="CSV Export" />
</form>

</body>
</html>

<?php 

	if (isset($_GET['month']) && is_numeric($_GET['month'])) {
		

		include_once 'config/database.php';
		include_once 'objects/sale.php';

		// db conection
		$database = new Database();
		$conn = $database->getConnection();

		// create sale object to interact with Sales table
		// $sale = new Sale($conn);


		// month string, yyyy-mm-dd
		$month = "2018-".$_GET['month']."-01";

		$query = "SELECT 
			    sd.sale_id,sd.product_id,p.product_name,sd.quantity,s.sale_date
			FROM
			    sales_details sd
			        LEFT JOIN
			    sales s ON sd.sale_id = s.sale_id
			    	LEFT JOIN
			    products p ON sd.product_id = p.product_id
			WHERE s.sale_date 
			BETWEEN '".$month."'
			AND '".$month."' + INTERVAL 1 MONTH
			ORDER BY s.sale_date	
		";

		// echo $query;
		// echo "<br>";
		$result = $conn->query($query);
		// echo $result->num_rows;



		// if successful query print table
		if ($result) {

			echo "<h2>";

			echo "
				<table border=1 cellpadding=\"4\">
					<thead>
						<th>Sale Date</th>
						<th>Sale ID</th>
						<th>Product ID</th>
						<th>Product Name</th>
						<th>Quantity</th>
					</thead>
					<tbody>
			";

			while ($r = $result->fetch_assoc()) {
				
				// print_r($r);
				// echo "<br>";


				echo "
					<tr>
						<td>".explode(" ",$r['sale_date'])[0]."</td>
						<td>".$r['sale_id']."</td>
						<td>".$r['product_id']."</td>
						<td>".$r['product_name']."</td>
						<td>".$r['quantity']."</td>
					</tr>
				";


			}

			echo "</tbody></table>";

			// if(isset($_POST['export'])) {
			// 	// ob_end_clean();
			// 	header('Content-Type: text/csv; charset=utf-8');
   //     			header('Content-Disposition: attachment; filename=data.csv');
   //      		$output = fopen('php://output', 'w');
   //      		fputcsv($output, array('Sale Date', 'Sale ID', 'Product ID', 'Product Name', 'Quantity'));
			// 	$result2 = mysqli_query($conn, $query);

			// 	while($row = mysqli_fetch_assoc($result2)) {
			// 		fputcsv($output, $row);
			// 	}
		
			// 	exit();
			// 	// echo $query;
			// 	// echo "<br>";
			// 	fclose($output);

				
			}
		}
		else {
			echo "Error: $conn->error";
		}

	}
?>