<?php 
class Database {
	// db credentials
	private $host 	= "localhost";
	private $db 	= "DP2_PHP";
	private $user 	= "root";
	private $pass 	= "";
	public $conn;

	// get db connection
	public function getConnection() {
		$this->conn = new mysqli($this->host, $this->user, $this->pass, $this->db);

		if ($this->conn->connect_error) {
			echo "Failed to connect to MySQL: " . $this->conn->connect_error;
		}

		return $this->conn;
	}
}
?>