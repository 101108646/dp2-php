<?php
	////
	$_POST = json_decode(file_get_contents('php://input'),true);
	$errmsg = "";

	// if sale id is not set or it is not a number
	if (!isset($_POST['sale_id']) || !is_numeric($_POST['sale_id'])) {
		$errmsg .= "Please enter a valid sale ID<br>\n";
	}

	echo $errmsg;

	if ($errmsg == "") {
		// db config and objects
		include_once 'config/database.php';
		include_once 'objects/sale.php';

		// db conection
		$database = new Database();
		$conn = $database->getConnection();

		// create sale object to interact with Sales table
		$sale = new Sale($conn);
		$sale->id = $_POST['sale_id'];

		$sale->delete();
	}

?>