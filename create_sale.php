<?php
	$errmsg = "";

	// decode posted json data 
	$_POST = json_decode(file_get_contents('php://input'),true);
	$productId = [];
	$productQuantity = [];
	

	for($i = 0; $i < count($_POST); $i++) {
		array_push($productId, $_POST[$i]['productId']);
		array_push($productQuantity, $_POST[$i]['quantity']);
	 }

	 for($i = 0; $i < count($productId); $i++) {
        if(!isset($productId[$i]) || !is_numeric($productId[$i])) {
            $errMsg .= "Product ID is not valid";
        }
    }
    for($i = 0; $i < count($productQuantity); $i++) {
        if(!isset($productQuantity[$i]) || !is_numeric($productQuantity[$i])) {
            $errMsg .= "Quantity is not valid";
        }
    }

	echo $errmsg;

	// if there are no error messages
	if ($errmsg == "") {

		// db config and objects
		include_once 'config/database.php';
		include_once 'objects/sale.php';

		// db connection
		$database = new Database();
		$conn = $database->getConnection();

		// create sale object to interact with Sales table
		$sale = new Sale($conn);
		// set sale data
		for($i = 0; $i < count($productId); $i++) {
            $sale->prod_id[$i] = $productId[$i];
			$sale->quantity[$i] = $productQuantity[$i];
		}
		
		$sale->create();
	}
?>