<?php
class Sale {
	// db connection
	private $conn;
	private $table_name = "Sales";

	// properties
	public $sale_id;
	public $prod_id;
	// public $date;
	public $quantity;

	public function __construct($db) {
		$this->conn = $db;
	}

	// create sale
	function create() {
		
		$query = 	"INSERT INTO SALES" . "
					SET sale_date=NOW()";
		$sql = "";
		
		$stockErr = "";
		$sufficientStock = TRUE;
		
		//this loop creates one string called $sql which is all of the queries for each of the products in the sale. 
		for($i = 0; $i < count($this->prod_id); $i++) {
			$insertProdId = $this->prod_id[$i];
			$insertQuantity = $this->quantity[$i]; 

			// select product name and stock from products table
			$stockQuery = "SELECT product_name, product_stock FROM Products
				WHERE product_id=$insertProdId";
			$stockResult = $this->conn->query($stockQuery);

			// if query was successful
			if ($stockResult) {
				// get stock and calculate remaining stock after sale
				$product = $stockResult->fetch_assoc();
				$stock = intval($product["product_stock"]);
				$name = $product["product_name"];
				$stockRemaining = $stock - $insertQuantity;

				// if sufficient stock proceed with query
				if ($stockRemaining >= 0) {
					$sql .= "INSERT INTO sales_details(sale_id, product_id, quantity) VALUES(LAST_INSERT_ID(), $insertProdId, $insertQuantity);";
					$sql .= "UPDATE Products SET product_stock=$stockRemaining WHERE product_id=$insertProdId;";
					// echo "sufficient stock";
				}
				else {
					// set var to false and append error msg
					$sufficientStock = FALSE;
					// echo "not enough stock";
					$stockErr .= "Product: $name has insufficient stock for sale - $stock remaining\n\r";
				}
			}
			else {
				echo "Error checking stock: " . $this->conn->error;
			}
		} 

		// if all items have sufficent stock, execute query
		if ($sufficientStock) {
			if ($this->conn->query($query) === TRUE && $this->conn->multi_query($sql)) {
				echo "New sale created successfully";
			}
			else {
				echo "Error: " . $query . "<br>" . $this->conn->error;
			}
		}
		else {
			// else output error msg
			echo "Sale unsuccessful: Insufficient stock\n\r";
			echo $stockErr;
		}
	}

	//This function returns the specific data we need to display sales records (sale id, date, total items, and order total)
	//it's different from the readAll function, which should return raw data
	function displaySalesRecords() {
		$query = $query = "SELECT s.sale_id, s.sale_date, SUM(sd.quantity) as TotalItems, SUM(sd.quantity * p.product_price) as orderTotal
		FROM sales s
		INNER JOIN sales_details sd
			on s.sale_id = sd.sale_id
		INNER JOIN products p
			on sd.product_id = p.product_id
		GROUP BY s.sale_id	
		";

		$result = $this->conn->query($query);
		// prepare array
		$rows = array();

		if ($result->num_rows > 0) {
		// add all rows to array
		while ($r = $result->fetch_assoc()) {
			$rows[] = $r;
		}

			// print array of rows as json
		echo json_encode($rows);
		}
		else {
			echo "No sales records found.";
		}				
	}

	// read all sales
	function readAll() {
		// echo "fetching sales";
		$query = "SELECT 
			    sd.sale_id,sd.product_id,p.product_name,sd.quantity,s.sale_date
			FROM
			    sales_details sd
			        LEFT JOIN
			    sales s ON sd.sale_id = s.sale_id
			    	LEFT JOIN
			    products p ON sd.product_id = p.product_id
			ORDER BY s.sale_date	
		";
		$result = $this->conn->query($query);
		// prepare array
		$rows = array();

		if ($result->num_rows > 0) {
			// add all rows to array
			while ($r = $result->fetch_assoc()) {
				$rows[] = $r;
			}

			// print array of rows as json
			echo json_encode($rows);
			return $rows;
		}
		else {
			echo "No sales records found.";
		}

		

	}
	
	

	// read a sale by id
	function read() {
		$query = "SELECT * FROM Sales WHERE sale_id=$this->id";
		$result = $this->conn->query($query);

		// if record is found, return as json
		// else return error msg
		if ($result->num_rows > 0) {
			echo json_encode($result->fetch_assoc());
		}
		else {
			echo "Error retrieving record #$this->id: record does not exist";
		}
	}

	// delete a sale by id
	function delete() {
	
		$query1 = "DELETE FROM sales_details WHERE sale_id=$this->id";
		$query2 = "DELETE FROM sales WHERE sale_id=$this->id";
		if ($this->conn->query($query1) && $this->conn->query($query2)) {
			echo "Record #$this->id deleted successfully";			
		}
		
		else {
			echo "Error deleting record #$this->id: record does not exist";
		}
	}
}
?>