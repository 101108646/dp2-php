
<?php
    //Just a testing file for exporting a database as a CSV file. Will need to be modified when the reporting functionality has been completed
    include_once 'config/database.php';
    include 'generate-report.php';


    if(isset($_POST['export']) && isset($_POST['month'])) {
        $database = new Database();
        $conn = $database->getConnection();
        
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=data.csv');
        ob_end_clean();
        $output = fopen('php://output', 'w');
        // fputcsv($output, array('Sale ID', 'Sale Date', 'Total Items', 'Order Total'));
        fputcsv($output, array('Sale Date', 'Sale ID', 'Product ID', 'Product Name', 'Quantity'));


        $month = "2018-".$_POST['month']."-01";

		$query = "SELECT 
			    -- sd.sale_id,sd.product_id,p.product_name,sd.quantity,s.sale_date 
                s.sale_date, s.sale_id, p.product_id, p.product_name, sd.quantity
			FROM
			    sales_details sd
			        LEFT JOIN
			    sales s ON sd.sale_id = s.sale_id
			    	LEFT JOIN
			    products p ON sd.product_id = p.product_id
			WHERE s.sale_date 
			BETWEEN '".$month."'
			AND '".$month."' + INTERVAL 1 MONTH
			ORDER BY s.sale_date	
        ";

        

        
        $result2 = mysqli_query($conn, $query);
        while($row = mysqli_fetch_assoc($result2)) {
            fputcsv($output, $row);
        }
        fclose($output);

    }

?>

